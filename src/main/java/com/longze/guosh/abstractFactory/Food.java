package com.longze.guosh.abstractFactory;

public abstract class Food {
    abstract void printName();
}
