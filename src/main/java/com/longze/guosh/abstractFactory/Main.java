package com.longze.guosh.abstractFactory;

public class Main {
    public static void main(String[] args) {
        AbstractFactory r=new ModernFactory();
        Vehicle c=r.createVehicle();
        c.go();
        Weapon w=r.createWeapon();
        w.shoot();
        Food f=r.createFood();
        f.printName();
    }
}
