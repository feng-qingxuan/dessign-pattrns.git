package com.longze.guosh.abstractFactory;

public abstract class Vehicle {
    abstract void go();
}
