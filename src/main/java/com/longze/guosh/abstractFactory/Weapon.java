package com.longze.guosh.abstractFactory;

public abstract class Weapon {
    abstract void shoot();
}

