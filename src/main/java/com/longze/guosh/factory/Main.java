package com.longze.guosh.factory;

public class Main {
    public static void main(String[] args) {
        Moveable m=new CarFactory().createCar();
        m.go();
    }
}
