package com.longze.guosh.factory;

public interface Moveable {
    void go();
}
