package com.longze.guosh.singleton;

/**
 * lazy loading
 * 懒汉式
 * 虽然达到了按需初始化目的，但也带来了线程不安全问题
 */
public class Mgr03 {
    private static Mgr03 INSTANCE;
    private Mgr03(){
    }
    private static Mgr03 getInstance(){
        if(INSTANCE==null){
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            INSTANCE=new Mgr03();
        }
        return INSTANCE;
    }

    public void m(){
        System.out.println("mmm");;
    }
    public static void main(String[] args) {
        for(int i=0;i<100;i++){
            new Thread(()->{
                System.out.println(Mgr03.getInstance().hashCode());
            }).start();
        }
    }
}
