package com.longze.guosh.singleton;

/**
 * 静态内部类写法-最完美的写法/
 * JVM保证单例
 * 懒加载
 */
public class Mgr06 {
    private Mgr06(){

    }
    private static class Mgr06Hold{
        private final static Mgr06 INSTANCE=new Mgr06();
    }
    private static Mgr06 getInstance(){
        return Mgr06Hold.INSTANCE;
    }
    public void m(){
        System.out.println("mmm");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                System.out.println(Mgr06.getInstance().hashCode());
            }).start();
        }
    }
}
