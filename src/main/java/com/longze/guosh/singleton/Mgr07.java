package com.longze.guosh.singleton;

/**
 * 完美中的完美 Perfect
 */
public enum Mgr07 {
    INSTANCE;

    public void m(){
        System.out.println("nnnn");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                System.out.println(Mgr07.INSTANCE.hashCode());
            }).start();
        }
    }
}
