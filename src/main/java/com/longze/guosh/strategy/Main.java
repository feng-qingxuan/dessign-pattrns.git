package com.longze.guosh.strategy;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Cat[] cs={new Cat(3,3),new Cat(5,5),new Cat(1,1),new Cat(10,10)};
        Dog[] ds= {new Dog(8),new Dog(5),new Dog(10),new Dog(1)};
        //comparater
        DogSorter dogsorter=new DogSorter();
        dogsorter.sort(ds);
        System.out.println("Dogs==="+Arrays.toString(ds));

        //comparator
        Sorter<Cat> catsorter=new Sorter<>();
        catsorter.sort(cs,new CatHeightComparator());
        System.out.println("Cat==="+Arrays.toString(cs));

    }
}
